<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<meta charset="utf-8">
        <title>{{ (isset($page_title) && $page_title) ? $page_title : config('app.name') }}</title>
        @if (isset($page_description) && $page_description)
        	 <meta name="description" content="{{ $page_description }}">
        @endif
        @if (isset($page_keywords) && $page_keywords)
        	 <meta name="keywords" content="{{ $page_keywords }}">
        @endif
		@include('stage::header')
		@include('scene-bootstrap-views::layouts.scene-bootstrap.style')
	</head>
	<body class="d-flex flex-column">
	
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
			<div class="container">
				<a class="navbar-brand" href="#">{{ config('app.name') }}</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
		
				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					@include('scene-bootstrap-views::layouts.scene-bootstrap.elements.navbar-left')
					@include('scene-bootstrap-views::layouts.scene-bootstrap.elements.navbar-right')
				</div>
			</div>
		</nav>

		<main class="flex-grow-1">
			@include('stage::content')
		</main>
		<footer>
			@include('scene-bootstrap-views::layouts.scene-bootstrap.elements.footer')
		</footer>
        @stack('stage::code')
    </body>
</html>
