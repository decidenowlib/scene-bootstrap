<?php

namespace DecideNow\SceneBootstrap;

use Illuminate\Support\ServiceProvider;

class SceneBootstrapServiceProvider extends ServiceProvider
{	
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
		//
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
		$this->loadViewsFrom(__DIR__.'/Views/scene', 'scene.bootstrap');
		$this->loadViewsFrom(__DIR__.'/Views/vendor', 'scene-bootstrap-views');
		$this->mergeConfigFrom(__DIR__.'/Config/scene_extensions.php', 'scene.extensions');
		$this->publishes([
			__DIR__.'/Assets' => public_path(),
		], 'scene-bootstrap-assets');
		$this->publishes([
			__DIR__.'/Views/vendor' => resource_path('views/vendor/scene'),
		], 'scene-bootstrap-views');
    }
}
