# Scene Bootstrap #

## Описание ##
Расширение для библиотеки Scene для Laravel, позволяющее использовать Bootstrap 4.

## Установка ##
Исходный код проекта "живёт" на Bitbucket, поэтому для начала необходимо подключить репозиторий, а затем добавить пакет:

```
composer config repositories.decidenow.scene.bootsrap vcs https://bitbucket.org/decidenowlib/scene-bootstrap
composer require decidenow/scene-bootstrap
```

Файлы Bootstrap должны размещаться в общедоступном каталоге, поэтому из необходимо опубликовать:
```
php artisan vendor:publish --tag=scene-bootstrap-assets
```

## Начало работы ##
Можно использовать все возможности Bootstrap 4.

Для дальнейшей работы желательно (но не обязательно) настроить основной шаблон.

1.
Опубликовать исходный вариант:
```
php artisan vendor:publish --tag=scene-bootstrap-views
```
2.
Изменить параметр _default_layout_ в файле _config/scene.php_ на _vendor.scene.layouts.scene-bootstrap.layout_

*В принципе, наличие папки vendor в папке с представлениями позволяет автоматически заменять имеющиеся представления на реализацию, предложенную разработчиком пакета. Это удобно.*
*Но основной макет приложения - это такая вещь, которую не желательно заменять автоматически (или даже полуавтоматически).*
*Автор ресширения рекомендовал бы скопировать папку views/vendor/scene/layouts/scene-bootstrap непосредственно в папку views и перенастроить переменную default_layout в файле конфигурации*

## Автор ##
Соколов Александр

E-mail: sanya_sokolov@inbox.ru
